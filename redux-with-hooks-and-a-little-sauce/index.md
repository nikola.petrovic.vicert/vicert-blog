# Redux With Hooks And A Little Sauce

![Sauce](sauce.jpg)

There is almost a year we are playing with React's new _hooks_ concept and it added simpler way or writing React components. We do not need to write _class_ any more in order to create React statefull component. In the following example as illustration will be used simple terminal app. User should enter command and press **Enter** to execute it. Here is a sample code that illustrate how easy it is to implement this using hooks:

```(JavaScript)
import React, { useState } from "react";
import "./MyTerminal.scss";
import { HEADER, READY } from "./CommodoreHeader";

const MyTerminal = props => {
  const [currCommand, setCurrCommand] = useState("");
  const [commands, setCommands] = useState([]);

  const onKeyPressHandler = ({ key, target }) => {
    if (key === "Enter" && target.value.trim()) {
      const c = [...commands];
      c.push(target.value);
      target.value = "";
      setCommands(c);
      setCurrCommand("");
    }
  };

  const onChangeHandler = ({ key, target }) => {
    setCurrCommand(target.value.toUpperCase());
  };

  return (
    <div className="terminal">
      <div className="commands">
        <div className="header">
          {HEADER.map((h, i) => (
            <div key={i}>{h}&nbsp;</div>
          ))}
          <div>&nbsp;</div>
          <div className="ready">{READY}</div>
        </div>
        {commands.map((command, idx) => (
          <div key={idx}>{command}</div>
        ))}
      </div>
      <div className="command-line">
        <input
          onKeyPress={onKeyPressHandler}
          value={currCommand}
          onChange={onChangeHandler}
        />
      </div>
    </div>
  );
};
```

Here is defined an array of `commands` that will hold all previously executed commands in terminal. When **Enter** key is pressed then new command will be pushed to array. Additionally, terminal by default prints all caps and `onChangeHandler` will ensure it really does.

With a little styling this should look like this:

![Simple terminal component](screen-01.gif)

Now we're going to move this to _Redux_ store by adding _action types_, _action creators_ and _reducer_:

**NOTE: _reduxsauce_ is used here to simplify this proces (for more reference, please see: https://github.com/jkeam/reduxsauce)**

```(JavaScript)
// Actions.js

import { createActions } from "reduxsauce";

const { Types, Creators } = createActions(
  {
    execute: ["cmd"]
  },
  {}
);

export { Types };
export default Creators;
```

```(JavaScript)
// Types.js

import { createTypes } from "reduxsauce";

export default createTypes("EXECUTE", {});
```

And finally, reducer:

```(JavaScript)
// reducer.js

import Types from "./Types";
import { createReducer } from "reduxsauce";

const INITIAL_STATE = {
  commands: []
};

export const execute = (state = INITIAL_STATE, action) => {
  return { ...state, commands: [...state.commands, action.cmd] };
};

// map our action types to our reducer functions
export const HANDLERS = {
  [Types.EXECUTE]: execute
};

export default createReducer(INITIAL_STATE, HANDLERS);
```

Now, we are going to tweak `MyTerminal` to use redux store instead a local one. I don't like spoiling the code by adding `connect()` method directly in our component class but rather creating wrapper around our component `MyTerminal` and connect it to _Redux_ store. Here is how should look our wrapper:

```(JavaScript)
import React from "react";
import { connect } from "react-redux";
import ActionCreators from "../test/Actions";
import MyTerminal from "./MyTerminal";

export default connect(null, ActionCreators)(MyTerminal);
```

It's time to update `MyTerminal` component to use _Redux_ store. Actually, there are just two small changes. The first, we are going to remove `const [commands, setCommands] = useState([]);` because we won't use it any more locally but rather via selector. And the second, we are going to add `useSelector()` to query our global _Redux_ state and take a list of already executed commands. Furthermore, now we are going to dispatch action that we mapped to `props` by calling `props.execute(...)` Here is how it should look now:

```(JavaScript)
  ...

  import { useSelector } from "react-redux";

  ...

  const commands = useSelector(state => {
    return state.commands || [];
  });

  const onKeyPressHandler = ({ key, target }) => {
    if (key === "Enter" && target.value.trim()) {
      props.execute(target.value.trim());
      target.value = "";
      setCurrCommand("");
    }
  };
```

And that's it! We have our terminal connected to _Redux_ store! To polish our terminal app, we can simulate result of execution by adding well known `?SYNTAX ERROR` and `PRESS PLAY ON TAPE` in `reducer.js`:

```(JavaScript)
// reducer.js
...

const resolveMessage = cmd => {
  switch (cmd) {
    case "LOAD":
      return { blocked: true, msg: "PRESS PLAY ON TYPE" };
    default:
      return { blocked: false, msg: "?SYNTAX ERROR" };
  }
};

const execute = (state = INITIAL_STATE, action) => {
  const commandsResult = [...state.commands, action.cmd];
  commandsResult.push("");
  const { blocked, msg } = resolveMessage(action.cmd);
  commandsResult.push(msg);
  if (!blocked) {
    commandsResult.push("READY");
  }

  return { ...state, commands: commandsResult, blocked };
};
...
```

Now we can use `blocked` flag to disable further command executions.

```(JavaScript)
// MyTerminal.js
...

const { commands, blocked } = useSelector(state => {
    return { commands: state.commands || [], blocked: state.blocked };
  });

...

  // render()

  {!blocked && (
    <div className="command-line">
      <input
        onKeyPress={onKeyPressHandler}
        value={currCommand}
        onChange={onChangeHandler}
      />
    </div>
  )}

...
```

This is the result of our work:

![Final](screen-02.gif)
